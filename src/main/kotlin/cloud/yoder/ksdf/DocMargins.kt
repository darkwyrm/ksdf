package cloud.yoder.ksdf

class DocMargins(var left: DocUnit = DocUnit(20_000), var top: DocUnit = DocUnit(20_000),
    var right: DocUnit = DocUnit(20_000), var bottom: DocUnit = DocUnit(20_000)) {

    constructor(left: Long, top: Long, right: Long, bottom: Long):
        this(DocUnit(left), DocUnit(top), DocUnit(right), DocUnit(bottom))

    companion object {

        fun fromString(s: String): DocMargins? {
            val out = DocMargins()

            if (s.isEmpty() || !reMarginInfoFormat.matches(s)) return null
            for (pair in s.split(",")) {
                val data = pair.split("=")
                val value = DocUnit.fromString(data[1]) ?: continue

                when (data[0]) {
                    "all" -> {
                        out.left.value = value.value
                        out.top.value = value.value
                        out.right.value = value.value
                        out.bottom.value = value.value
                    }
                    "left" -> out.left = value
                    "top" -> out.top = value
                    "right" -> out.right = value
                    "bottom" -> out.bottom = value
                }
            }

            return out
        }
    }
}
