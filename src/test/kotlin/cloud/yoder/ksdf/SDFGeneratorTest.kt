package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.Style
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertNotNull

class SDFGeneratorTest {

    @Test
    fun generateSmokeTest() {

        val parser = SDFParser.parseFromString("""[document][head][/head][body][/body][/document]""")
        assert(parser.messages.size == 0)

        val doc = SDFGenerator(parser).generate()
        assert(doc != null)
    }

    @Test
    fun generateHeadHandlerTest() {
        val lexer = SDFLexer().process("""[document]
            [head]
                [title]Document Title[/title]
                [meta type="authors"]Author1,Author2[/meta]
                [meta type="description"]Document description[/meta]
            [/head]
            [body][/body][/document]""")
        val parser = SDFParser()
        parser.process(lexer.tokens)
        parser.validate()
        parser.makeCompliant()

        assert(parser.messages.size == 0)

        val doc = SDFGenerator(parser).generate()
        assertNotNull(doc)

        // First, validate all the document metadata

        assert(doc.doctype == "sftm")
        assert(doc.version == "1.0")
        assert(doc.language == "en")
        assert(doc.title == "Document Title")
        assert(doc.authors.size == 2)
        assert(doc.authors[0] == "Author1")
        assert(doc.authors[1] == "Author2")
        assert(doc.description == "Document description")
    }

    @Test
    fun generateBodyHandlerTest() {
        val lexer = SDFLexer().process("""[document]
            [head][title]Document Title[/title][/head]
            |[body]
            |something extra
            |[h1]Main Heading[/h1]
            |
            |[p]This is the first paragraph[/p]
            |[p]Second paragraph[/p]
            |
            |[/body][/document]""".trimMargin())
        val parser = SDFParser()
        parser.process(lexer.tokens)
        parser.validate()
        parser.makeCompliant()

        assert(parser.messages.size == 0)

        val doc = SDFGenerator(parser).generate()
        assertNotNull(doc)

        println(doc)

        // Again we validate all the document metadata

        assert(doc.doctype == "sftm")
        assert(doc.version == "1.0")
        assert(doc.language == "en")
        assert(doc.title == "Document Title")

        assert(doc.blocks.size == 4)
        assert(doc.blocks[0].contentType == BlockContentType.TEXT)
        assert((doc.blocks[0] as TextContent).spans[0].text == "something extra")
        assert((doc.blocks[0] as TextContent).spans.size == 1)
        assert(doc.blocks[1].contentType == BlockContentType.HEADING)
        assert((doc.blocks[1] as HeadingBlock).spans.size == 1)
        assert(doc.blocks[2].contentType == BlockContentType.TEXT)
        assert((doc.blocks[2] as TextContent).spans[0].text == "This is the first paragraph")
        assert(doc.blocks[3].contentType == BlockContentType.TEXT)
        assert((doc.blocks[3] as TextContent).spans[0].text == "Second paragraph")
    }

    @Test
    fun headTest() {
        val lexer = SDFLexer().process("""[document type="sdf" version="1.0" lang="es"]
            [head]
            |   [title]Documento de Prueba[/title]
            |   [meta type="size"]a4[/meta]
            |   [meta type="authors"]Corbin Simons,z3rowon[/meta]
            |   [meta type="description"]Un documento de muestra que se parece a uno real[/meta]
            |   [meta type="keywords"]ejemplo,ejemplo2[/meta]
            |   [meta type="margins"]top=1in,bottom=1in[/meta]
            |[/head]
            |[body][/body][/document]""".trimMargin())
        val parser = SDFParser()
        parser.process(lexer.tokens)
        parser.validate()
        parser.makeCompliant()

        if (parser.messages.size > 0) {
            for(msg in parser.messages)
                println(msg)
        }
        assert(parser.messages.size == 0)

        val doc = SDFGenerator(parser).generate()
        assertNotNull(doc)
        println(doc)

        // Again we validate all the document metadata

        assert(doc.doctype == "sdf")
        assert(doc.version == "1.0")
        assert(doc.language == "es")
        assert(doc.title == "Documento de Prueba")

        assert(doc.authors.size == 2)
        assert(doc.authors[0] == "Corbin Simons")
        assert(doc.authors[1] == "z3rowon")

        assert(doc.description == "Un documento de muestra que se parece a uno real")

        assert(doc.keywords.size == 2)
        assert(doc.keywords[0] == "ejemplo")
        assert(doc.keywords[1] == "ejemplo2")

        assertNotNull(doc.size)
        assert(doc.size!!.width.value == 210_000L)
        assert(doc.size!!.height.value == 297_000L)

        assertEquals(20.0, doc.margins.left.toMM())
        assertEquals(1.0, doc.margins.top.toInches())
        assertEquals(20.0, doc.margins.right.toMM())
        assertEquals(1.0, doc.margins.bottom.toInches())

        assert(doc.blocks.size == 0)
    }

    @Test
    fun paragraphTest1() {
        val lexer = SDFLexer().process("""[document][body]
            |[p]This is the first paragraph. It contains some [b]bold text[/b], some 
            |[i]italicized text[/i], and some [u]underlined text[/u]. It also contains a 
            |[a url="https://start.duckduckgo.com"]link to DuckDuckGo[/a].[/p]
            |[/body][/document]""".trimMargin())
        val parser = SDFParser()
        parser.process(lexer.tokens)
        parser.validate()
        parser.makeCompliant()

        if (parser.messages.size > 0) {
            for(msg in parser.messages)
                println(msg)
        }
        assert(parser.messages.size == 0)

        val doc = SDFGenerator(parser).generate()
        assertNotNull(doc)
        println(doc)

        assertEquals(doc.blocks.size, 1)
        assertEquals(doc.blocks[0].contentType, BlockContentType.TEXT)

        val p = doc.blocks[0] as TextContent
        assertEquals(9, p.spans.size)

        assertEquals("This is the first paragraph. It contains some ", p.spans[0].text)
        assertEquals("bold text", p.spans[1].text)
        assertEquals(", some ", p.spans[2].text)
        assertEquals("italicized text", p.spans[3].text)
        assertEquals(", and some ", p.spans[4].text)
        assertEquals("underlined text", p.spans[5].text)
        assertEquals(". It also contains a ", p.spans[6].text)
        assertEquals("link to DuckDuckGo", p.spans[7].text)
        assertEquals(".", p.spans[8].text)

        assert(p.spans[1].style!!.properties == Style().setBold(true).properties)
        assert(p.spans[3].style!!.properties == Style().setItalic(true).properties)
        assert(p.spans[5].style!!.properties == Style().setUnderline(true).properties)

        assertIs<LinkSpan>(p.spans[7])
        assertEquals("https://start.duckduckgo.com", (p.spans[7] as LinkSpan).url.toString())
    }


    @Test
    fun paragraphTest2() {
        // This test exists to test style handling and inheritance

        val docString = """[document type="sdf" version="1.0"][body]""" +
                """[p style="align: justify;"][b]Some bold and [i]italicized[/i] text[/b][/p]""" +
                """[/body][/document]"""
        val parser = SDFParser.parseFromString(docString)
        val doc = SDFGenerator(parser).generate()
        assertNotNull(doc)
        assertEquals(1, doc.blocks.size)

        val p = doc.blocks[0] as TextContent
        assertEquals(3, p.spans.size)
        assertEquals(1, p.style!!.properties.size)
        assert(p.style!!.properties.containsKey("align"))
        assertEquals("justify", p.style!!.properties["align"])

        assertEquals("Some bold and ", p.spans[0].text)
        p.spans[0].style!!.properties.run {
            assertEquals(2, size)
            assert(containsKey("align"))
            assertEquals("justify", get("align"))
            assert(containsKey("font-weight"))
            assertEquals("700", get("font-weight"))
        }

        assertEquals("italicized", p.spans[1].text)
        p.spans[1].style!!.properties.run {
            assertEquals(3, size)
            assert(containsKey("align"))
            assertEquals("justify", get("align"))
            assert(containsKey("font-weight"))
            assertEquals("700", get("font-weight"))
            assert(containsKey("font-style"))
            assertEquals("italic", get("font-style"))
        }

        assertEquals(" text", p.spans[2].text)
        p.spans[2].style!!.properties.run {
            assertEquals(2, size)
            assert(containsKey("align"))
            assertEquals("justify", get("align"))
            assert(containsKey("font-weight"))
            assertEquals("700", get("font-weight"))
        }
    }
}