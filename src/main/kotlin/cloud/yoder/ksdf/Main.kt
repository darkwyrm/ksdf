package cloud.yoder.ksdf

fun main() {
    val document = "[document][body]" +
        "[h1]This is my heading[/h1]\n\n" +
        "This is some of the document's body text.\n" +
        "[/body][/document]"
    val parser = SDFParser.parseFromString(document)
    for (child in parser.root.children) {
        println(child)
    }

    if (parser.messages.isNotEmpty()) println("---------------------")
    for (msg in parser.messages)
        println(msg.msg)
}