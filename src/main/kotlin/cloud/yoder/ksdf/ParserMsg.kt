package cloud.yoder.ksdf

enum class MessageType {
    WARNING,
    ERROR,
    FATAL,
}

enum class ParserCode {

    // Fatal errors -- these often indicate the existence bugs somewhere in the code
    BAD_DOCUMENT_TAG_COUNT,
    BAD_BODY_TAG_COUNT,

    // Parser errors -- these may or may not be a problem, depending on the security context
    EMPTY_DOCUMENT,
    ILLEGAL_TAG_NAME,
    UNCLOSED_TAG,
    BAD_TAG_LOCATION,
    MULTIPLE_TITLE_TAGS,

    // Parser warnings -- the presence of warnings usually indicates that the document may not be
    // rendered as the author originally intended
    IGNORED_CONTENT,
    BAD_PARAMETER,

    EMPTY_TAG,
    UNCLOSED_TAG_PAIR,
    UNOPENED_TAG_PAIR,
    BAD_NESTING,

    // Seeing this one in production is almost certainly a bug
    UNRECOGNIZED_TOKEN,
}

data class ParserMsg (val type: MessageType, val code: ParserCode, val msg: String, val line: Int) {
    var node: Node? = null
}

fun pWarn(msg: String, msgCode: ParserCode, line: Int): ParserMsg {
    return ParserMsg(MessageType.WARNING, msgCode, "$msg in line $line", line)
}

fun pError(msg: String, msgCode: ParserCode, line: Int): ParserMsg {
    return ParserMsg(MessageType.ERROR, msgCode, "$msg in line $line", line)
}
