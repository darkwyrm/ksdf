package cloud.yoder.ksdf.style

enum class ScriptPosition {
    NORMAL,
    SUPERSCRIPT,
    SUBSCRIPT;

    override fun toString(): String {
        return when(this) {
            NORMAL -> "normal"
            SUPERSCRIPT -> "superscript"
            SUBSCRIPT -> "subscript"
        }
    }

    fun fromString(s: String): ScriptPosition? {
        return when(s) {
            "normal" -> NORMAL
            "superscript" -> SUPERSCRIPT
            "subscript" -> SUBSCRIPT
            else -> null
        }
    }
}

enum class NodeType {
    BLOCK,
    SPAN,
    ROOT, // only for the document instance itself
}