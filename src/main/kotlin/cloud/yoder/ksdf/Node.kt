package cloud.yoder.ksdf

enum class NodeType {
    ROOT,
    TAG,
    TEXT
}

class Node(val type: NodeType, val token: Token) {
    val children = ArrayList<Node>()
    var parent: Node? = null
    var isClosed = false
    val params = HashMap<String, String>()

    fun addChild(child: Node) {
        child.parent = this
        children.add(child)
    }

    fun walk(walkFunc: (node: Node, index: Int, siblings: ArrayList<Node>) -> Unit) {
        for (i in 0 until children.size) {
            walkFunc(children[i], i, children)
        }
    }

    override fun toString(): String {
        if (type == NodeType.TAG) {
            val sb = StringBuilder()
            if (params.size > 0) {
                sb.append("[${token.value}")
                for (param in params) {
                    sb.append(" ${param.key}=\"${param.value}\"")
                }
                sb.append("]")
            } else {
                sb.append("[${token.value}]")
            }
            for (child in children)
                sb.append(child.toString())
            if (isClosed)
                sb.append("[/${token.value}]")
            return sb.toString()
        }

        return token.value
    }

    fun findLastOpenTag(name: String): Node? {
        var currentParent: Node? = this
        while (currentParent != null) {
            for (child in children.reversed()) {
                if (child.type == NodeType.TAG && child.token.value == name && !child.isClosed) {
                    return child
                }
            }

            // By checking the parent node for a match, we potentially save quite a few
            // iterations in the next level up the hierarchy
            if (currentParent.type == NodeType.TAG && currentParent.token.value == name &&
                !currentParent.isClosed) {
                return currentParent
            }
            currentParent = currentParent.parent
        }
        return null
    }

    fun findOpenParent(): Node? {
        var currentParent: Node? = this
        while (currentParent != null) {
            if (currentParent.type == NodeType.TAG && !currentParent.isClosed) {
                return currentParent
            }
            currentParent = currentParent.parent
        }
        return null
    }
}

fun findChildTag(parent: Node, tag: String, recursive: Boolean): Node? {
    // First check the immediate children of the parent tag
    for (child in parent.children) {
        if (child.type != NodeType.TAG) continue
        if (child.token.value == tag) return child
    }

    if (!recursive) return null

    for (child in parent.children) {
        if (child.type != NodeType.TAG) findChildTag(child, tag, true)
    }
    return null
}

fun countChildTag(parent: Node, tag: String, recursive: Boolean): Int {

    var childCount = 0
    for (child in parent.children) {
        if (child.type != NodeType.TAG) continue
        if (child.token.value == tag) childCount++
    }

    if (!recursive) return childCount

    for (child in parent.children) {
        if (child.type == NodeType.TAG) childCount += countChildTag(child, tag, true)
    }
    return childCount
}

fun rootNode(): Node { return Node(NodeType.ROOT, Token(TokenType.EMPTY, "", -1)) }

fun textNode(token: Token): Node { return Node(NodeType.TEXT, token) }

fun tagNode(token: Token): Node { return Node(NodeType.TAG, token) }