package cloud.yoder.ksdf

import org.junit.Test

class ParserTest {

    @Test
    fun tagParserTest1() {
        val document = """[document][head]"""
        val lexer = SDFLexer().process(document)
        val parser = SDFParser().process(lexer.tokens)
        assert(parser.messages.size == 0)

        assert(parser.root.children.size == 1)
        assert(parser.root.children[0].children.size == 1)
    }

    @Test
    fun tagParserTest2() {
        val document = """[document][head][/head][/document]"""
        val lexer = SDFLexer().process(document)
        val parser = SDFParser().process(lexer.tokens)
        assert(parser.messages.size == 0)

        assert(parser.root.children.size == 1)
        assert(parser.root.children[0].type == NodeType.TAG)
        val docTag = parser.root.children[0]
        assert(docTag.isClosed)

        assert(docTag.children.size == 1)
        assert(docTag.children[0].type == NodeType.TAG)
        val headTag = docTag.children[0]
        assert(headTag.isClosed)
    }

    @Test
    fun basicParserTest1() {
        val document = """[document][head][/head][body]text content[/body][/document]"""
        val lexer = SDFLexer().process(document)
        val parser = SDFParser().process(lexer.tokens)
        assert(parser.messages.size == 0)

        assert(parser.root.children.size == 1)
        val docTag = parser.root.children[0]

        assert(docTag.children.size == 2)
        for (child in parser.root.children) {
            assert(child.type == NodeType.TAG)
            assert(child.isClosed)
        }
        val body = docTag.children[1]
        assert(body.children.size == 1)
        assert(body.children[0].type == NodeType.TEXT)
    }
}