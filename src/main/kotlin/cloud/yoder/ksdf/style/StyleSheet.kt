package cloud.yoder.ksdf.style

/**
 * This library provides a CSS-like styling experience, but it is limited compared to the full CSS spec.
 *
 * For now, it supports only tags, IDs, and classes for selectors. Nesting is also not supported at this time.
 */
class StyleSheet {
    private var styles = mutableMapOf<String, Style>()

    override fun toString(): String {
        val sb = StringBuilder("StyleSheet[")
        styles.forEach{ sb.append("\n    ${it.key}: ${it.value}")}
        sb.append("\n]")

        return sb.toString()
    }

    fun get(selector: String): Style? { return styles[selector] }

    fun set(selector: String, style: Style): StyleSheet { styles[selector] = style; return this }

    fun delete(selector: String): StyleSheet { styles.remove(selector); return this }

    fun has(selector: String): Boolean { return styles.containsKey(selector) }

    /**
     * addStyle() is a builder-style method for easily building and adding styles to the stylesheet
     */
    fun addStyle(selector: String): Style {
        val out = Style()
        styles[selector] = out
        return out
    }

    companion object {
        fun defaultDark(): StyleSheet {
            val out = genericDefaults()
            out.addStyle(":root")
                .setFontFamily("'Noto Sans', Arial, Helvetica, sans-serif")
                .setFontSize(12)
                .setBGColor(32, 32, 32)
                .setTextColor(255, 255, 255)

            return out
        }

        fun defaultLight(): StyleSheet {
            val out = genericDefaults()
            out.addStyle(":root")
                .setFontFamily("'Noto Sans', Arial, Helvetica, sans-serif")
                .setFontSize(12)
                .setBGColor(255, 255, 255)
                .setTextColor(0, 0, 0)

            return out
        }

        fun defaultPrint(): StyleSheet {
            val out = genericDefaults()
            out.addStyle(":root")
                .setFontFamily("'Noto Serif', 'Times New Roman', Times, serif")
                .setFontSize(12)
                .setBGColor(255, 255, 255)
                .setTextColor(0, 0, 0)

            return out
        }

        private fun genericDefaults(): StyleSheet {
            val out = StyleSheet()

            out.addStyle("body")
                .addProperty("padding", "12.7mm")

            out.addStyle("h1")
                .setFontFamily("\"Noto Sans\", Arial, Helvetica, sans-serif")
                .setBold(true)
                .setFontSize(18)
                .addProperty("margin-top", "12pt")
                .addProperty("margin-bottom", "6pt")

            out.addStyle("h2")
                .setFontFamily("\"Noto Sans\", Arial, Helvetica, sans-serif")
                .setBold(true)
                .setFontSize(16)
                .addProperty("margin-top", "10pt")
                .addProperty("margin-bottom", "6pt")

            out.addStyle("h3")
                .setFontFamily("\"Noto Sans\", Arial, Helvetica, sans-serif")
                .setBold(true)
                .setFontSize(14)
                .addProperty("margin-top", "7pt")
                .addProperty("margin-bottom", "6pt")

            out.addStyle("h4")
                .setFontFamily("\"Noto Sans\", Arial, Helvetica, sans-serif")
                .setBold(true)
                .setFontSize(13)
                .addProperty("margin-top", "6pt")
                .addProperty("margin-bottom", "6pt")

            out.addStyle("h5")
                .setFontFamily("\"Noto Sans\", Arial, Helvetica, sans-serif")
                .setBold(true)
                .setFontSize(12)
                .addProperty("margin-top", "6pt")
                .addProperty("margin-bottom", "3pt")

            out.addStyle("h6")
                .setFontFamily("\"Noto Sans\", Arial, Helvetica, sans-serif")
                .setBold(true)
                .setFontSize(10)
                .addProperty("margin-top", "3pt")
                .addProperty("margin-bottom", "3pt")

            out.addStyle("p")
                .addProperty("margin-top", "10pt")
                .addProperty("margin-bottom", "10pt")
                .addProperty("line-height", "1.5em")

            out.addStyle("a")
                .setTextColor(0x27, 0x5a, 0x90)
                .setBold(true)
                .setUnderline(true)

            out.addStyle("ol")
                .addProperty("text-indent", "1em")

            out.addStyle("ul")
                .addProperty("text-indent", "1em")

            out.styles = out.styles.toMutableMap()

            return out
        }
    }
}
