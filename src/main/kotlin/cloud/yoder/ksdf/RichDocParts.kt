package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.StyleNode
import cloud.yoder.ksdf.style.Style
import cloud.yoder.ksdf.style.StyleSheet
import java.net.URL

/**
 * A Block is the top-level unit of layout in a FormattedDocument.
 */
open class Block(parent: StyleNode?, val contentType: BlockContentType, name: String = "", style: Style? = null):
    StyleNode(name, parent, style)

/**
 * TextContent instances are just regular paragraphs of text
 */
class TextContent(parent: StyleNode?, name: String = "", style: Style? = null): Block(parent, BlockContentType.TEXT,
    name, style) {

    var spans = mutableListOf<Span>()

    /**
     * Convenience function which adds span of text to a TextContent with an optional style parameter. If the style is
     * null, the span inherits its style from the TextContent. If the TextContent also has a null style, the block will
     * inherit its style from either style specified at the document level or the default document style
     */
    fun addText(text: String, textStyle: Style? = null): TextContent {
        spans.add(Span(text, this, "", textStyle))

        return this
    }

    /**
     * Convenience function similar to addText() except that it adds a hyperlink to the block
     */
    fun addLink(text: String, url: String, linkStyle: Style? = null): TextContent {
        spans.add(LinkSpan(text, url, this, "", linkStyle))

        return this
    }

    override fun toString(): String {
        val sb = StringBuilder("TextContent[")

        when {
            spans.size == 1 -> sb.append("${style?.toString() ?: "(null)"} | ${spans[0]} ")
            spans.size > 1 -> {
                sb.append("\n  ${style?.toString() ?: "(null)"}")
                for (span in spans)
                    sb.append("\n\t$span ")
                sb.append("\n]")
            }
            else -> sb.append("]")
        }

        return sb.toString()
    }

    override fun updateEffectiveStyle() {

        val defaults = findStyleSheet() ?: throw NullPointerException(
            "BUG: null stylesheet in TextContent::updateEffectiveStyle()")

        effectiveStyle = calcEffective(defaults)

        spans.forEach { it.parentStyleChanged(defaults) }
    }
}

/**
 * HeadingBlock instances represent headings like h1, etc. Headings are internally the same basic thing as regular
 * paragraphs, but in order to have some automatic padding management of source code vs the rendered output, headings
 * need to be their own thing in the document model.
 */
class HeadingBlock(parent: StyleNode?, name: String = "", style: Style? = null): Block(parent, BlockContentType.HEADING,
    name, style) {

    var spans = mutableListOf<Span>()

    fun addText(text: String, textStyle: Style? = null): HeadingBlock {
        spans.add(Span(text, this, "", textStyle))

        return this
    }

    fun addLink(text: String, url: String, linkStyle: Style? = null): HeadingBlock {
        spans.add(LinkSpan(text, url, this, "", linkStyle))

        return this
    }

    override fun toString(): String {
        val sb = StringBuilder("HeadingBlock[")
        sb.append(style?.toString() ?: "(null)")
        for (span in spans)
            sb.append("$span ")
        sb.append("]")

        return sb.toString()
    }

    override fun updateEffectiveStyle() {

        val defaults = findStyleSheet() ?: throw NullPointerException(
            "BUG: null stylesheet in HeadingBlock::updateEffectiveStyle()")

        effectiveStyle = calcEffective(defaults)

        spans.forEach { it.parentStyleChanged(defaults) }
    }
}

/**
 * RuleBlock instances represent horizontal rules -- the equipvalent of the HTML <hr> tag. These blocks contain no
 * content, but can be styled.
 */
class RuleBlock(parent: StyleNode?, name: String = "", style: Style? = null): Block(parent, BlockContentType.RULE, name,
    style) {

    override fun toString(): String {
        return "HRule[${style?.toString() ?: "(null)"}]"
    }
}

/**
 * A Span is a unit of text and an optional associated style
 */
open class Span(var text: String = "", parent: StyleNode?, name: String = "", spanStyle: Style? = null):
    StyleNode(name, parent, spanStyle) {

    override fun toString(): String {
        val content = if (text.isEmpty()) "\"\""
        else "\"$text\"".replace("\n", "\\n")

        return "Span[${style?.toString() ?: "(null)"} | $content]"
    }

    override fun parentStyleChanged(defaults: StyleSheet) { updateEffectiveStyle() }
}

/**
 * A LinkSpan represents a hyperlink
 */
class LinkSpan(text: String = "", urlStr: String = "", parent: StyleNode?, name: String = "", spanStyle: Style? = null):
    Span(text, parent, name, spanStyle) {

    var url = URL(urlStr)

    override fun toString(): String {
        val content = if (text.isEmpty()) "\"\""
        else "\"$text\"".replace("\n", "\\n")

        return "LinkSpan[${style?.toString() ?: "(null)"}($url) | $content]"
    }
}

enum class BlockContentType {
    TEXT,
    IMAGE,
    LIST,
    TABLE,
    CODE,
    HEADING,
    RULE,
    EMPTY;

    override fun toString(): String {
        return when(this) {
            TEXT -> "text"
            IMAGE -> "image"
            LIST -> "list"
            TABLE -> "table"
            CODE -> "code"
            HEADING -> "heading"
            RULE -> "rule"
            EMPTY -> "empty"
        }
    }
}

