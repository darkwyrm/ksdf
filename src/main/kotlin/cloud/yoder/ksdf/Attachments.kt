package cloud.yoder.ksdf

import java.io.ByteArrayInputStream
import javax.activation.MimeType
import javax.imageio.ImageIO

abstract class Attachment(var id: String, var mime: MimeType = MimeType()) {

    fun setID(s: String): Attachment { id = s; return this }

    companion object {
        /**
         * Instantiate a new Attachment-based object from the supplied ID, MIME type, and data.
         * If any of these arguments are empty, the call will fail. The call will return an
         * object specific to the type of data passed to it, if one exists. If no type-specific
         * class exists, a RawAttachment instance will be returned.
         */
        fun fromData(id: String, mime: String, data: ByteArray): Attachment? {
            if (id.isEmpty() || mime.isEmpty() || data.isEmpty()) return null

            // TODO: Finish implementing Attachment::fromData()

            return when (mime) {
                else -> RawAttachment(id, data)
            }
        }
    }
}

class RawAttachment(id: String, var data: ByteArray):
    Attachment(id, MimeType("application/octet-stream"))

class ImageAttachment: Attachment("", MimeType()) {

    fun fromData(id: String, data: ByteArray, convert: Boolean): ImageAttachment? {
        var img = ImageIO.read(ByteArrayInputStream(data))
        
        TODO("Finish implementing ImageAttachment::fromData")
    }
}
