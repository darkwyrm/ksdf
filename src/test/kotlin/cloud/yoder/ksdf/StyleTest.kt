package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.Style
import cloud.yoder.ksdf.style.StyleSheet
import org.junit.Test
import java.util.TreeMap

class StyleTest {

    @Test
    fun toStringTest() {
        val treeMap = TreeMap<String,String>()
        treeMap["padding-bottom"] = "25pt"
        treeMap["padding-top"] = "25pt"
        var style = Style("someID", mutableListOf("p", "blockquote"), treeMap)

        val styleStr = listOf("Style[id=\"someID\"", "class=\"p, blockquote\"",
            "style=\"padding-bottom: 25pt; padding-top: 25pt;\"").joinToString(separator = " ") + "]"
        assert(style.toString() == styleStr)

        style = Style("someID")
        assert(style.toString() == "Style[id=\"someID\"]")

        style = Style("", mutableListOf("p", "blockquote"))
        assert(style.toString() == "Style[class=\"p, blockquote\"]")

        style = Style("", mutableListOf(), treeMap)
        assert(style.toString() == "Style[style=\"padding-bottom: 25pt; padding-top: 25pt;\"]")
    }

    @Test
    fun modificationTests() {
        val treeMap = TreeMap<String,String>()
        treeMap["padding-bottom"] = "25pt"
        var style = Style("someID", mutableListOf("p"), treeMap)

        val styleClone = style.clone()
        styleClone.properties.clear()

        assert(styleClone.toString() == "Style[id=\"someID\" class=\"p\"]")
        assert(style.toString() != styleClone.toString())

        val treeMap2 = TreeMap<String,String>()
        treeMap["padding-top"] = "25pt"
        var style2 = Style("id2", mutableListOf("blockquote"), treeMap2)

        style.append(style2)
        var styleStr = listOf("Style[id=\"someID\"", "class=\"p, blockquote\"",
            "style=\"padding-bottom: 25pt; padding-top: 25pt;\"").joinToString(separator = " ") + "]"
        assert(style.toString() == styleStr)

        treeMap["padding-bottom"] = "5pt"
        styleStr = listOf("Style[id=\"id2\"", "class=\"p, blockquote\"",
            "style=\"padding-bottom: 25pt; padding-top: 25pt;\"").joinToString(separator = " ") + "]"
        style = Style("someID", mutableListOf("p"), treeMap)
        style2.properties["padding-bottom"] = "25pt"
        style2.properties["padding-top"] = "25pt"
        style.update(style2)
        assert(style.toString() == styleStr)
        style2.properties.remove("padding-bottom")

        style = Style("someID", mutableListOf("p"), treeMap)
        style2 = Style("id2", mutableListOf("blockquote"), treeMap2)
        style.syncFrom(style2)
        assert(style.styleID == "id2")
        assert(style.classes.size == 1 && style.classes[0] == "blockquote")
        assert(style.properties.size == 1)
        assert(style.properties.containsKey("padding-top"))
        assert(style.properties["padding-top"] == "25pt")
    }
}