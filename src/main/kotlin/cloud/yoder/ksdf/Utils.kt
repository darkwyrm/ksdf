package cloud.yoder.ksdf

private val gTagList = arrayOf(
    // Document structure
    "document", "head", "body", "attachments",

    // Section-specific
    "meta", "attachment", "style",

    // Block-level content and formatting
    "div", "p", "hr", "img", "ul", "ol", "li",
    "h1", "h2", "h3", "h4", "h5", "h6",

    // Span-level content and formatting
    "a", "b", "code", "i", "span", "sub", "sup", "u",

    // Tables
    "table", "thead", "tr", "th", "tbody", "td", "tfoot",

    // SDF-specific
    "signature", "encryption",
)

/**
 * Returns true if the tag name given is one of the tags defined in the spec
 */
private fun isValidTagName(tag: String): Boolean {
    return tag in gTagList
}

/**
 * Returns true if the tag name given is a block-level tag
 */
fun isBlockTag(tag: String): Boolean {
    return tag in arrayOf("img", "div", "p", "h1", "h2", "h3", "h4", "h5", "h6", "ol", "ul", "li", "table", "hr")
}

/**
 * Returns true if the tag name represents some type of span of text
 */
fun isSpanTag(tag: String): Boolean {
    return tag in arrayOf("a", "b", "code", "i", "span", "sub", "sup", "u")
}

/**
 * Returns true if the tag name given is a heading
 */
fun isHeadingTag(tag: String): Boolean {
    return tag in arrayOf("h1", "h2", "h3", "h4", "h5", "h6")
}

internal val reWhitespace = Regex("\\s+")
internal val reLeadingNewlines = Regex("^[\r\n]+")
internal val reTrailingNewlines = Regex("[\r\n]+$")
internal val reDocUnitFormat = Regex("^[\\d.]+ *(in|mm|pt|pc)$")
internal val reMarginInfoFormat = Regex("^[a-z]\\w*=[\\d.]+ *(in|mm|pt|pc)(,[a-z]\\w*=[\\d.]+ *(in|mm|pt|pc))*\$")
