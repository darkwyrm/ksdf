package cloud.yoder.ksdf.style

import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment

/**
 * The Style class represents the CSS-like styling for RichDocument element. It provides support for IDs, classes,
 * and regular style properties
 */
class Style(var styleID: String = "", var classes: MutableList<String> = mutableListOf(),
            var properties: MutableMap<String, String> = HashMap()) {

    override fun toString(): String {
        val sb = StringBuilder("Style[")
        if (styleID.isNotEmpty())
            sb.append("id=\"$styleID\"")

        if (classes.isNotEmpty()) {
            if (styleID.isNotEmpty()) sb.append(" ")
            sb.append("class=\"${classes.joinToString(separator = ", ")}\"")
        }

        if (properties.isNotEmpty()) {
            if (styleID.isNotEmpty() || classes.isNotEmpty()) sb.append(" ")
            sb.append("style=\"${this.propertyString()}\"")
        }

        sb.append("]")
        return sb.toString()
    }

    /**
     * This method returns all styling as a style string. It does not include a style class or ID
     */
    fun classString(): String {
        return if (classes.isEmpty()) ""
        else classes.joinToString(separator = ", ")
    }

    /**
     * This method returns all styling as a string. It does not include a style class or ID
     */
    fun propertyString(): String {
        return if (properties.isEmpty()) ""
        else
            properties.map { "${it.key}: ${it.value}" }.joinToString(separator = "; ") + ";"
    }

    /**
     * Builder-style method that adds any present values in the passed style to the current one. Note that this does
     * not overwrite any existing values in the current style, only adding to them. If passed a null value, this call
     * does nothing
     */
    fun append(from: Style?): Style {
        // style IDs need to be unique, so we won't be copying them here
        if (from == null) return this

        if (from.classes.isNotEmpty())
            classes.addAll(from.classes.filter { !classes.contains(it) })

        for (pair in from.properties) {
            if (!properties.containsKey(pair.key)) properties[pair.key] = pair.value
        }

        return this
    }

    /**
     * Builder-style method that overwrites any values in the current style with those that are present in the passed
     * one and adds any style classes not present in the current object. It also overwrites the style ID. This does not,
     * however, delete any styling not present in the passed style. If given a null value, this call does nothing.
     */
    fun update(from: Style?): Style {
        if (from == null) return this

        if (from.styleID.isNotEmpty()) styleID = from.styleID
        if (from.classes.isNotEmpty())
            classes.addAll(from.classes.filter { !classes.contains(it) })

        for (pair in from.properties)
            properties[pair.key] = pair.value

        return this
    }

    /**
     * Builder-style method that completely overwrites the current style with the data from the passed one. If given a
     * null value, this call does nothing
     */
    fun syncFrom(from: Style?): Style {
        if (from == null) return this

        styleID = from.styleID
        classes = from.classes.toMutableList()
        properties = HashMap(from.properties)

        return this
    }

    /**
     * This method returns a copy of the current object
     */
    fun clone(): Style {
        return Style(styleID, classes.toMutableList(), properties.toMutableMap())
    }

    /**
     * A builder method which sets the id of the style
     */
    fun setID(id: String): Style { styleID = id; return this }

    /**
     * A builder method which adds a class to the style or changes the value if it already is present
     */
    fun addClass(c: String): Style {
        if (!classes.contains(c)) classes.add(c)
        return this
    }

    /**
     * A builder method which removes a class from the style
     */
    fun removeClass(c: String): Style { classes.remove(c); return this }

    /**
     * A builder method which adds a property to the style or changes the value if it already is present. This is a
     * generic method that functions much like type-specific methods like setBGColor().
     */
    fun addProperty(prop: String, propValue: String): Style { properties[prop] = propValue; return this }

    /**
     * A builder method which removes a property from the style. This is a generic method that functions much like
     * type-specific methods like setBGColor().
     */
    fun removeProperty(prop: String): Style { properties.remove(prop); return this }

    /** Sets the style's background color and returns itself */
    fun setBGColor(red: Int, green: Int, blue: Int): Style {
        properties["background-color"] = "rgb(${red}, ${green}, ${blue})"
        return this
    }

    /** Sets (or removes) the style's background color and returns itself */
    fun setBGColor(value: Color?): Style {
        if (value != null) {
            val red = (value.red * 255.0).toInt()
            val green = (value.green * 255.0).toInt()
            val blue = (value.blue * 255.0).toInt()
            properties["background-color"] = "rgb(${red}, ${green}, ${blue})"
        }
        else { properties.remove("background-color") }

        return this
    }

    /** Toggles bold on the style object and returns itself */
    fun setBold(value: Boolean): Style {
        if (value) { properties["font-weight"] = "700" }
        else { properties.remove("font-weight") }

        return this
    }

    /** Toggles code on the style object and returns itself */
    fun setCode(value: Boolean, lang: String? = null): Style {
        if (value) { properties["-sftm-code"] = lang ?: "unspecified" }
        else { properties.remove("-sftm-code") }

        return this
    }

    /**
     * Sets (or removes) the style's font family and returns itself. Passing an empty string will result in the font
     * family value being set to null.
     */
    fun setFontFamily(value: String?): Style {
        if (value != null) { properties["font-family"] = value }
        else { properties.remove("font-family") }

        return this
    }

    /** Sets (or removes) the style's font size and returns itself. Note that the SFTM spec requires text to be */
    fun setFontSize(value: Int?): Style {
        if (value != null) { properties["font-size"] = "${value}pt" }
        else { properties.remove("font-size") }

        if (value != null) {
            if (value in 8..999)
                properties["font-size"] = "${value}pt"
            else
                properties.remove("font-size")

        } else { properties.remove("font-size") }

        return this
    }

    /** Toggles italics on the style object and returns itself */
    fun setItalic(value: Boolean): Style {
        if (value) { properties["font-style"] = "italic" }
        else { properties.remove("font-style") }

        return this
    }

    fun isEmpty(): Boolean {
        return properties.isEmpty() && styleID.isEmpty() && classes.isEmpty()
    }

    fun isNotEmpty(): Boolean {
        return properties.isNotEmpty() || classes.isNotEmpty() || styleID.isNotEmpty()
    }

    /** Sets (or removes) the script position (subscript, etc.) on the style object and returns itself */
    fun setPosition(value: ScriptPosition): Style {
        when (value) {
            ScriptPosition.SUPERSCRIPT -> properties["vertical-align"] = "super"
            ScriptPosition.SUBSCRIPT -> properties["vertical-align"] = "sub"
            ScriptPosition.NORMAL -> properties.remove("vertical-align")
        }

        return this
    }

    /** Sets the style's text color and returns itself */
    fun setTextColor(red: Int, green: Int, blue: Int): Style {
        properties["color"] = "rgb(${red}, ${green}, ${blue})"
        return this
    }

    /** Sets (or removes) the style's text color and returns itself */
    fun setTextColor(value: Color?): Style {
        if (value != null) {
            val red = (value.red * 255.0).toInt()
            val green = (value.green * 255.0).toInt()
            val blue = (value.blue * 255.0).toInt()
            properties["color"] = "rgb(${red}, ${green}, ${blue})"
        }
        else { properties.remove("color") }

        return this
    }

    /** Toggles underlining on the style object and returns itself */
    fun setUnderline(value: Boolean): Style {
        if (value) { properties["text-decoration-line"] = "underline" }
        else { properties.remove("text-decoration-line") }

        return this
    }

    /**
     * Sets the text alignment property. Passing null to this method removes the style. This is a block style, and
     * applying it to a span will do nothing.
     */
    fun setAlignment(align: TextAlignment?): Style {
        when (align) {
            TextAlignment.LEFT -> properties["text-align"] = "left"
            TextAlignment.CENTER -> properties["text-align"] = "center"
            TextAlignment.RIGHT -> properties["text-align"] = "right"
            TextAlignment.JUSTIFY -> properties["text-align"] = "justify"
            else -> properties.remove("text-align")
        }

        return this
    }

    companion object {

        /**
         *  Returns a Style object from a string which represents a tag name. It returns null if (a) the tag doesn't
         *  have an associated style or (b) if the string isn't a valid tag
         */
        fun fromTag(tag: String, params: Map<String, String>): Style? {
            val out =  when (tag) {
                "b" -> Style().setBold(true)
                "i" -> Style().setItalic(true)
                "u" -> Style().setUnderline(true)
                "code" -> {
                    val lang = if (params.containsKey("language")) params["language"] else "undefined"
                    Style().setCode(true, lang)
                }
                "sub" -> Style().setPosition(ScriptPosition.SUBSCRIPT)
                "sup" -> Style().setPosition(ScriptPosition.SUPERSCRIPT)

                // Most tags can take at least a `style` parameter. Top-level structure tags, do not, however.
                "hr", "img", "li", "ol", "p", "span", "ul", "table", "tr", "td", "thead", "tbody", "tfoot" -> Style()

                // Tags that don't take a style parameter or invalid tags
                else -> return null
            }

            for (param in params) {
                when (param.key) {
                    "bgcolor" -> {
                        try { out.setBGColor(Color.web(param.value)) }
                        catch(_: Exception) {
                            // If the color is invalid, we don't want to do anything to handle it. The bgColor
                            // attribute will remain null and if this is the only formatting bit here, null will be
                            // returned
                        }
                    }
                    "color" -> {
                        try { out.setTextColor(Color.web(param.value)) }
                        catch(_: Exception) {
                            // Like with the background color, don't do anything if it's an invalid color
                        }
                    }
                    "font" -> { out.setFontFamily(param.value) }
                    "fontsize" -> { out.setFontSize(param.value.toIntOrNull()) }
                    "style" -> { out.properties.putAll(splitCSSString(param.value)) }
                }
            }

            return out
        }
    }
}

/**
 * This utility function turns a string of CSS properties and returns them as a set of key-value pairs
 */
fun splitCSSString(s: String): Map<String,String> {

    if (s.isEmpty()) return mapOf()

    val out: MutableMap<String, String> = mutableMapOf()
    for (item in s.split(";")) {
        val pair = item.trim().split(":")

        if (pair.size != 2 || pair[0].isEmpty()) continue

        out[pair[0].trim()] = pair[1].trim()
    }

    return out
}