package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.Style
import javax.activation.MimeType
import javax.activation.MimeTypeParseException

/**
 * An SDFGenerator takes a parser and generates a RichDocument object from it
 */
class SDFGenerator(val parser: SDFParser) {
    val messages = mutableListOf<DocMsg>()

    fun generate(): RichDocument? {
        val out = RichDocument()
        out.clear()
        messages.clear()

        if (parser.docTag == null) {
            dFatal(GeneratorCode.NO_DOCUMENT_TAG, "A document tag is required")
            return null
        }

        if (!handleDocTag(out)) return null

        // When encryption and signature support are implemented, the checks and processing for those features will go
        // here

        if (parser.headTag != null) handleHeadTag(out)

        if (parser.bodyTag == null) {
            dFatal(GeneratorCode.NO_BODY_TAG, "A body tag is required")
            return null
        }

        if (!handleBodyTag(out)) return null

        if (parser.attTag != null) handleAttachments(out)

        return out
    }

    private fun handleDocTag(doc: RichDocument): Boolean {
        if (parser.docTag == null) {
            dFatal(GeneratorCode.NO_DOCUMENT_TAG, "A document tag is required")
            return false
        }

        // There isn't much to this method just because all we need to do is turn parameters into instance field values
        for (param in parser.docTag!!.params) {
            when (param.key) {
                "type" -> doc.doctype = param.value
                "version" -> doc.version = param.value
                "lang" -> doc.language = param.value
            }
        }

        return true
    }

    private fun handleHeadTag(doc: RichDocument) {
        if (parser.headTag == null) return

        var widthTag = ""
        var heightTag = ""
        var sizeTag = ""

        for (child in parser.headTag!!.children) {
            // We deliberately ignore any bare text directly underneath [head] tags. Bare head text is illegal
            // according to the spec. By excluding everything TagNode, we gracefully handle parser bugs that might put
            // non-TagNode instance in the node tree.
            if (child.type != NodeType.TAG)
                continue

            when (child.token.value) {
                "meta" -> {
                    if (!child.params.containsKey("type")) {
                        dError(GeneratorCode.MISSING_REQUIRED_PARAMETER, "meta tag missing type parameter")
                        continue
                    }
                    if (child.params["type"]!!.isEmpty()) {
                        dError(GeneratorCode.EMPTY_REQUIRED_PARAMETER, "meta tag has empty type parameter")
                        continue
                    }
                    if (child.children.size == 0) {
                        dWarn(GeneratorCode.CONTENT_MISSING, "meta tag ${child.params["type"]!!} has empty value")
                        continue
                    }
                    if (child.children[0].type != NodeType.TEXT) {
                        dWarn(GeneratorCode.BUG,
                            "BUG: meta tag child node has type ${child.children[0].javaClass}")
                        continue
                    }

                    when (child.params["type"]) {
                        "description" -> doc.description = child.children[0].token.value
                        "authors" -> {
                            doc.authors = child.children[0].token.value
                                .split(",")
                                .map { it.trim() }
                                .toMutableList()
                        }
                        "keywords" -> {
                            doc.keywords = child.children[0].token.value
                                .split(",")
                                .map { it.trim() }
                                .toMutableList()
                        }
                        "width" -> { widthTag = child.children[0].token.value }
                        "height" -> { heightTag = child.children[0].token.value }
                        "size" -> { sizeTag = child.children[0].token.value }
                        "margins" -> {
                            val marginInfo = DocMargins.fromString(child.children[0].token.value)
                            if (marginInfo != null) {
                                doc.margins = marginInfo
                            } else {
                                dError(GeneratorCode.BAD_DOCUMENT_METADATA, "Invalid margin information")
                            }
                        }
                        else -> doc.otherMeta[child.params["type"]!!] = child.children[0].token.value
                    }
                }
                "title" -> {
                    if (child.children.size == 0) {
                        dWarn(GeneratorCode.CONTENT_MISSING, "Title tag is empty. It should be removed if not used.")
                        continue
                    }
                    if (child.children[0].type != NodeType.TEXT) {
                        dWarn(GeneratorCode.BUG,
                            "BUG: title tag child node has type ${child.children[0].javaClass}")
                        continue
                    }
                    doc.title = child.children[0].token.value
                }
                "style" -> {
                    // Style information is not validated here -- we're just focusing on converting the AST to a
                    // RichDocument instance
                    if (child.children.size == 0) {
                        dWarn(GeneratorCode.CONTENT_MISSING,
                            "Style tag is empty. It should be removed if not used.")
                        continue
                    }
                    if (child.children[0].type != NodeType.TEXT) {
                        dWarn(GeneratorCode.BUG,
                            "BUG: style tag child node has type ${child.children[0].javaClass}")
                        continue
                    }
                    doc.rawStyleInfo = child.children[0].token.value
                }
                else -> {
                    dError(GeneratorCode.ILLEGAL_TAG_FOR_LOCATION,
                        "Tag ${child.token} may not be used in the document head")
                    continue
                }
            }
        }

        if (sizeTag.isNotEmpty()) {
            // A preset document size overrides any specified custom document dimensions
            val docSize = DocSize.fromString(sizeTag)
            if (docSize != null)
                doc.size = docSize
            else
                dError(GeneratorCode.BAD_DOCUMENT_METADATA, "A bad or unsupported document size preset was used")
            return
        }
        val docWidth = DocUnit.fromString(widthTag)
        if (docWidth == null) {
            dError(GeneratorCode.BAD_DOCUMENT_METADATA, "Document uses a bad or unsupported width")
            return
        }
        val docHeight = DocUnit.fromString(heightTag)
        if (docHeight == null) {
            dError(GeneratorCode.BAD_DOCUMENT_METADATA, "Document uses a bad or unsupported height")
            return
        }

        doc.size = DocSize(docWidth, docHeight)
    }

    private fun handleBodyTag(doc: RichDocument): Boolean {
        doc.blocks.clear()
        processChildNodes(doc, parser.bodyTag!!, Style())
        return true
    }

    private fun processChildNodes(doc: RichDocument, parent: Node, style: Style): Boolean {
        for (child in parent.children) {
            when (child.type) {
                NodeType.TEXT -> {
                    val nlStripped = (child.toString() as CharSequence)
                        .replace(reLeadingNewlines, "")
                        .replace(reTrailingNewlines, "")
                    if (nlStripped.isEmpty())
                         continue

                    if (parent.token.value == "a") {
                        doc.appendLink(nlStripped, parent.params["url"] ?: "", style)
                    } else {
                        doc.appendText(nlStripped, style)
                    }
                }
                NodeType.TAG -> {
                    if (isSpanTag(child.token.value)) {
                        // Span tags inherit style information only from other span tags. This is because the
                        // RichTextDocument structure flattens span nesting, i.e. `[b]foo[i]bar[/i][/b]` into
                        // `[b]foo[/b][b][i]bar[/i][/b]`. So in the code below, a parent only passes on its style info
                        // if both it and its child are span tags.
                        val childStyle = Style.fromTag(child.token.value, child.params) ?: Style()
                        val completeStyle = if (isSpanTag(parent.token.value)) {
                            style.clone().update(childStyle)
                        } else {
                            childStyle
                        }
                        processChildNodes(doc, child, completeStyle)
                    }
                    else if (isBlockTag(child.token.value)) processBlockTag(doc, child, style)
                }
                NodeType.ROOT -> {
                    /* We should never be here */
                    return false
                }
            }
        }
        return true
    }

    private fun processBlockTag(doc: RichDocument, node: Node, inherited: Style) {
        // Right now we don't support nested blocks or non-text blocks

        val style = Style.fromTag(node.token.value, node.params)?.update(inherited)

        when (node.token.value) {
            "h1","h2","h3","h4","h5","h6" -> doc.blocks.add(HeadingBlock(doc, "", style))
            "hr" -> doc.blocks.add(RuleBlock(doc, "", style))
            else -> doc.blocks.add(TextContent(doc, "", style))
        }
        processChildNodes(doc, node, style ?: inherited)
    }

    // TODO: Move validation in handleAttachments() to SDFParser
    private fun handleAttachments(doc: RichDocument) {
        if (parser.attTag == null) return

        if (parser.attTag!!.children.size == 0) {
            dWarn(GeneratorCode.CONTENT_MISSING,
                "Attachments tag exists, but has no attachments. This should be removed if not used.")
            return
        }

        for (child in parser.attTag!!.children) {
            // Like with the document head, we ignore anything that isn't a tag
            if (child.type != NodeType.TAG) continue

            if (child.token.value != "attachment") {
                dError(GeneratorCode.ILLEGAL_TAG_FOR_LOCATION,
                    "Tag ${child.token.value} is not allowed inside the document's attachments section")
                continue
            }

            if (!child.params.containsKey("attid")) {
                dError(GeneratorCode.MISSING_REQUIRED_PARAMETER,
                    "Attachment is missing its attachment ID and will not be processed further")
                continue
            }
            if (child.params["attid"]!!.isEmpty()) {
                dError(GeneratorCode.EMPTY_REQUIRED_PARAMETER,
                    "Attachment has an empty attachment ID and will not be processed further")
                continue
            }

            if (!child.params.containsKey("type")) {
                dError(GeneratorCode.MISSING_REQUIRED_PARAMETER,
                    "Attachment ${child.params["attid"]!!} is missing its type and will not be processed further")
                continue
            }
            if (child.params["attid"]!!.isEmpty()) {
                dError(GeneratorCode.EMPTY_REQUIRED_PARAMETER,
                    "Attachment ${child.params["attid"]!!} has an empty type and will not be processed further")
                continue
            }

            try { MimeType(child.params["type"]) }
            catch (e: MimeTypeParseException) {
                dError(GeneratorCode.EMPTY_REQUIRED_PARAMETER,
                    "Attachment ${child.params["attid"]!!} has an invalid MIME type")
                MimeType()
            }

            if (child.children.size == 0) {
                dWarn(GeneratorCode.CONTENT_MISSING,"Attachment ${child.params["attid"]!!} has no content")
                continue
            }

            val rawData = try {
                Base85.rfc1924Decoder.decode(child.children[0].token.value)
            } catch (e: Exception) {
                dError(GeneratorCode.CONTENT_ERROR,
                    "Attachment ${child.params["attid"]!!} has invalid data and will not be processed further")
                continue
            }

            doc.attachments[child.params["attid"]!!] = Attachment.fromData(child.params["attid"]!!,
                child.params["type"]!!, rawData) ?: continue
        }
    }

    // These three convenience methods aren't strictly necessary, but they exist to make the processing code a little
    // less verbose to make the programming logic a little cleaner.
    private fun dWarn(msgCode: GeneratorCode, msg: String) { messages.add(DocMsg(MessageType.WARNING, msgCode, msg)) }
    private fun dError(msgCode: GeneratorCode, msg: String) { messages.add(DocMsg(MessageType.ERROR, msgCode, msg)) }
    private fun dFatal(msgCode: GeneratorCode, msg: String) { messages.add(DocMsg(MessageType.FATAL, msgCode, msg)) }
}

/**
 * GeneratorCode instances are for reporting status problems with creating RichDocument instances from parsed content
 */
enum class GeneratorCode {

    // Fatal errors
    NO_DOCUMENT_TAG,
    NO_BODY_TAG,

    // Non-Fatal errors
    ILLEGAL_TAG_FOR_LOCATION,
    MISSING_REQUIRED_PARAMETER,
    EMPTY_REQUIRED_PARAMETER,
    CONTENT_ERROR,  // Content errors can be warnings or errors, depending on context
    BAD_DOCUMENT_METADATA,

    // Warnings
    CONTENT_MISSING,
    BUG,
}

data class DocMsg (val type: MessageType, val code: GeneratorCode, val msg: String)

