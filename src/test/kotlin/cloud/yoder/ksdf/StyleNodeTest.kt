package cloud.yoder.ksdf

import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class StyleNodeTest {

    @Test
    fun testStyleInheritance() {

        val docString = """[document type="sdf" version="1.0"][body]""" +
                """[p style="align: justify;" color="blue"][b]Some bold and [i]italicized[/i] text[/b][/p]""" +
                """[/body][/document]"""
        val parser = SDFParser.parseFromString(docString)
        val doc = SDFGenerator(parser).generate()
        assertNotNull(doc)
        assertEquals(1, doc.blocks.size)

        val p = doc.blocks[0] as TextContent
        p.spans[0].style!!.properties.let {
            assertContains(it, "font-weight")
            assertEquals("700", it["font-weight"])

            assertEquals(false, it.containsKey("color"))
            assertEquals(false, it.containsKey("align"))
        }

        p.spans[0].effectiveStyle.properties.let {
            assertContains(it, "font-weight")
            assertEquals("700", it["font-weight"])
            assertContains(it, "color")
            assertEquals("rgb(0, 0, 255)", it["color"])
            assertContains(it, "align")
            assertEquals("justify", it["align"])
        }

        p.spans[1].style!!.properties.let {
            assertContains(it, "font-style")
            assertEquals("italic", it["font-style"])

            assertEquals(false, it.containsKey("color"))
            assertEquals(false, it.containsKey("align"))
        }

        p.spans[1].effectiveStyle.properties.let {
            assertContains(it, "font-weight")
            assertEquals("700", it["font-weight"])
            assertContains(it, "color")
            assertEquals("rgb(0, 0, 255)", it["color"])
            assertContains(it, "align")
            assertEquals("justify", it["align"])
            assertContains(it, "font-style")
            assertEquals("italic", it["font-style"])
        }

        p.spans[2].style!!.properties.let {
            assertContains(it, "font-weight")
            assertEquals("700", it["font-weight"])

            assertEquals(false, it.containsKey("color"))
            assertEquals(false, it.containsKey("align"))
        }

        p.spans[2].effectiveStyle.properties.let {
            assertContains(it, "font-weight")
            assertEquals("700", it["font-weight"])
            assertContains(it, "color")
            assertEquals("rgb(0, 0, 255)", it["color"])
            assertContains(it, "align")
            assertEquals("justify", it["align"])
        }

    }
}