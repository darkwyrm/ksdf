package cloud.yoder.ksdf.style

import kotlin.reflect.typeOf

/**
 * A StyleNode instance represents a styled document element. It provides the necessary context information for
 * determining how a RichTextDocument element should be styled. It is intended to be subclassed by RichTextDocument
 * components.
 */
open class StyleNode(var name: String, var parent: StyleNode?, var style: Style? = null) {

    private var styleSheet: StyleSheet? = null
    private var effectiveStyleBackend: Style? = null
    var effectiveStyle: Style
        get() {
            if (effectiveStyleBackend == null) updateEffectiveStyle()
            return effectiveStyleBackend!!
        }
        set(value) { effectiveStyleBackend = value }


    /** Hook function for recalculating the node's style in light of inherited styling but also the stylesheet. If the
     * node is not part of a StyleNode hierarchy that contains a stylesheet, this call does nothing.
     */
    open fun updateEffectiveStyle() {
        if (styleSheet == null) { styleSheet = findStyleSheet() ?: return }
        effectiveStyleBackend = calcEffective(styleSheet!!)
    }

    /**
     * Hook function for responding to changes in inherited styling. By default it updates the node's effective style
     */
    open fun parentStyleChanged(defaults: StyleSheet) { updateEffectiveStyle() }

    /**
     * Locates the default stylesheet and returns it if found
     */
    open fun findStyleSheet(): StyleSheet? {
        var currentParent = parent
        while (currentParent != null) {
            if (currentParent is StyleRootNode?) return currentParent.findStyleSheet()
            currentParent = currentParent.parent
        }
        return null
    }

    /**
     * Calculates the effective style based on a source style and stylesheet and returns a new Style instance
     * containing the result.
     */
    fun calcEffective(ss: StyleSheet): Style {
        val out = Style(style?.styleID ?: "", style?.classes?.toMutableList() ?: mutableListOf())

        // This method starts out by getting all relevant style information from the most general scope, the stylesheet
        // and works its way down into more and more specific scopes until it ends at styling applied at the node
        // itself

        // Stylesheet scope

        out.update(ss.get(":root"))

        // For each class in the StyleNode's style, pull in the different bits of style information for those classes
        // from the stylesheet.
        style?.classes?.forEach { out.update(ss.get(it)) }

        out.update(ss.get(name))
        out.update(ss.get(out.styleID))

        // Parent nodes

        // Rather than use recursion, we'll use a more iterative method that kind of simulates recursion by using
        // a list of StyleNode parents as a stack. We keep working our way up the document tree from the current
        // StyleNode instance until we reach the document root. Once we've reach the document root, we start checking
        // each style node in the list and applying any style information as needed.
        val nodesToProcess = mutableListOf<StyleNode>()
        var currentNode: StyleNode? = this
        while (currentNode != null) {
            nodesToProcess.add(0, currentNode)
            currentNode = currentNode.parent
        }
        nodesToProcess.forEach { out.update(it.style) }

        return out
    }
}

open class StyleRootNode(var styleSheet: StyleSheet): StyleNode("root", null, null) {
    override fun findStyleSheet(): StyleSheet { return styleSheet }
}
