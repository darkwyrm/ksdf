# KSDF

A library to read and write Safe Document Format (SDF) documents written in Kotlin and licensed under the Mozilla Public License v2.0.

## Description

This is the reference implementation of the [SDF specification](https://gitlab.com/darkwyrm/libsdf/-/blob/main/Specification.adoc).

## Contributing

I'm a relatively new Kotlin developer, so I'm still learning. Contributions are always welcome and appreciated.

## Project Status

This project is current under development and implementing document reading is the primary focus.