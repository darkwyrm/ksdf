package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.Style
import cloud.yoder.ksdf.style.StyleRootNode
import cloud.yoder.ksdf.style.StyleSheet
import java.util.TreeMap

/**
 * A RichDocument represents a rich text document with possibly other forms of embedded content
 */
class RichDocument: StyleRootNode(StyleSheet.defaultLight()) {
    // Although these three parameters are expected of SFTM/SDF documents, we have defaults if they are left out.
    var doctype = "sftm"
    var version = "1.0"
    var language = "en"
    var size: DocSize? = null
    var margins = DocMargins()

    var title = ""
    var authors = mutableListOf<String>()
    var description = ""
    var keywords = mutableListOf<String>()
    var otherMeta = mutableMapOf<String, String>()

    var rawStyleInfo = ""

    var blocks = mutableListOf<Block>()
    var attachments = TreeMap<String, Attachment>()

    /**
     * Empties a document instance
     */
    fun clear() {
        // Reset these to defaults and clear the others
        doctype = "sftm"
        language = "en"
        version = "1.0"

        authors.clear()
        description = ""
        keywords.clear()
        otherMeta.clear()
        blocks.clear()
    }

    override fun toString(): String {
        val sb = StringBuilder("Document[type=\"$doctype\" version=\"$version\"\n")
        if (title.isNotEmpty())
            sb.append("title: \"$title\"\n")
        if (authors.isNotEmpty())
            sb.append("authors: ${authors.joinToString(separator = ", ")}\n")

        if (description.isNotEmpty())
            sb.append("description: $description\n")
        if (keywords.isNotEmpty())
            sb.append("keywords: ${keywords.joinToString(separator = ", ")}\n")

        for (item in otherMeta)
            sb.append("${item.key}: ${item.value}\n")

        for (block in blocks)
            sb.append("${block}\n")
        sb.append("]")
        return sb.toString()
    }

    /**
     * Builder method which appends a block to the document. If given a null parameter, this call does nothing.
     */
    fun appendBlock(block: Block?): RichDocument {
        if (block == null) return this

        blocks.add(block)
        block.parent = this
        return this
    }

    /**
     * This method adds text to the end of the document. If the last block does not support adding text, a new block
     * is created and the supplied text is added to it.
     */
    fun appendText(text: String, style: Style? = null): RichDocument {

        val nlStripped = (text as CharSequence)
            .replace(reLeadingNewlines, "")
            .replace(reTrailingNewlines, "")
        if (blocks.isEmpty()) {
            blocks.add(TextContent(this).addText(nlStripped, style))
            return this
        }

        when (val lastBlock = blocks.last()) {
            is TextContent -> lastBlock.addText(nlStripped, style)
            is HeadingBlock -> lastBlock.addText(nlStripped, style)
            else -> blocks.add(TextContent(this).addText(nlStripped, style))
        }
        return this
    }

    /**
     * This method adds text to the end of the document. If the last block does not support adding text, a new block
     * is created and the supplied text is added to it.
     */
    fun appendLink(text: String, url: String, style: Style? = null): RichDocument {

        val nlStripped = (text as CharSequence)
            .replace(reLeadingNewlines, "")
            .replace(reTrailingNewlines, "")
        val urlStripped = (url as CharSequence)
            .replace(reLeadingNewlines, "")
            .replace(reTrailingNewlines, "")

        if (blocks.isEmpty()) {
            blocks.add(TextContent(this).addLink(nlStripped, urlStripped, style))
            return this
        }

        when (val lastBlock = blocks.last()) {
            is TextContent -> lastBlock.addLink(nlStripped, urlStripped, style)
            is HeadingBlock -> lastBlock.addLink(nlStripped, urlStripped, style)
            else -> blocks.add(TextContent(this).addLink(nlStripped, urlStripped, style))
        }
        return this
    }
}
