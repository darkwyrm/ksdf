package cloud.yoder.ksdf

// The PeekCharStream class gives us iterator-like functionality while also allowing us to peek
// the next character in the stream so that we can check for token boundaries
internal class PeekCharStream(input: String) {
    var data = input
        private set
    private lateinit var it: CharIterator
    private var current: Char? = null
    private var peek: Char? = null
    private var position = -1

    init { set(input) }

    fun set(input: String) {
        data = input
        it = input.iterator()
        current = if (it.hasNext()) it.nextChar() else null
        peek = if (it.hasNext()) it.nextChar() else null
        position = if (current != null) 0 else -1
    }

    // Get the current character
    fun getCurrent(): Char? { return current }
    fun getPosition(): Int { return position }

    // Check the value of the next character without moving the read position
    fun peekNext(): Char? { return peek }
    fun hasNext(): Boolean { return peek != null }

    // Moves the read pointer to the next character in the string
    fun moveNext(): Char? {
        current = peek
        peek = if (it.hasNext()) it.nextChar() else null
        position = if (current != null) position + 1 else -1

        return current
    }
}
