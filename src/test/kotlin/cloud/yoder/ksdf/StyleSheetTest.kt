package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.StyleSheet
import org.junit.Test

class StyleSheetTest {

    @Test
    fun testDefaults() {
        val dark = StyleSheet.defaultDark()
        assert(dark.has("h1"))
        assert(dark.get("h1")?.properties?.containsKey("margin-top") ?: false)
        assert(dark.has(":root"))
        assert(dark.get(":root")?.properties?.containsKey("color") ?: false)
        assert((dark.get(":root")?.properties?.get("color") ?: "") == "rgb(255, 255, 255)")
        assert(dark.get(":root")?.properties?.containsKey("background-color") ?: false)
        assert((dark.get(":root")?.properties?.get("background-color") ?: "") == "rgb(32, 32, 32)")

        val light = StyleSheet.defaultLight()
        assert(light.has("h1"))
        assert(light.get("h1")?.properties?.containsKey("margin-top") ?: false)
        assert(light.has(":root"))
        assert(light.get(":root")?.properties?.containsKey("color") ?: false)
        assert((light.get(":root")?.properties?.get("color") ?: "") == "rgb(0, 0, 0)")
        assert(light.get(":root")?.properties?.containsKey("background-color") ?: false)
        assert((light.get(":root")?.properties?.get("background-color") ?: "") == "rgb(255, 255, 255)")
    }
}