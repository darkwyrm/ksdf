package cloud.yoder.ksdf

/**
 * The DocSizePreset type makes it easy to specify common (and some not-so-common) metric
 * and imperial paper sizes.
 */
enum class DocSizePreset {
    A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10,
    B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, B10,
    C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10,
    D1, D2, D3, D4, D5, D6, D7,
    E3, E4, E5, E6,
    LETTER,
    LEGAL,
    LONGBOND,
    TABLOID;

    override fun toString(): String {
        return when (this) {
            A0 -> "a0"
            A1 -> "a1"
            A2 -> "a2"
            A3 -> "a3"
            A4 -> "a4"
            A5 -> "a5"
            A6 -> "a6"
            A7 -> "a7"
            A8 -> "a8"
            A9 -> "a9"
            A10 -> "a10"
            B0 -> "b0"
            B1 -> "b1"
            B2 -> "b2"
            B3 -> "b3"
            B4 -> "b4"
            B5 -> "b5"
            B6 -> "b6"
            B7 -> "b7"
            B8 -> "b8"
            B9 -> "b9"
            B10 -> "b10"
            C0 -> "c0"
            C1 -> "c1"
            C2 -> "c2"
            C3 -> "c3"
            C4 -> "c4"
            C5 -> "c5"
            C6 -> "c6"
            C7 -> "c7"
            C8 -> "c8"
            C9 -> "c9"
            C10 -> "c10"
            D1 -> "d1"
            D2 -> "d2"
            D3 -> "d3"
            D4 -> "d4"
            D5 -> "d5"
            D6 -> "d6"
            D7 -> "d7"
            E3 -> "e3"
            E4 -> "e4"
            E5 -> "e5"
            E6 -> "e6"
            LETTER -> "letter"
            LEGAL -> "legal"
            LONGBOND -> "longbond"
            TABLOID -> "tabloid"
        }
    }

    companion object {
        fun fromString(string: String): DocSizePreset? {
            return when (string.lowercase()) {
                "a0" -> A0
                "a1" -> A1
                "a2" -> A2
                "a3" -> A3
                "a4" -> A4
                "a5" -> A5
                "a6" -> A6
                "a7" -> A7
                "a8" -> A8
                "a9" -> A9
                "a10" -> A10
                "b0" -> B0
                "b1" -> B1
                "b2" -> B2
                "b3" -> B3
                "b4" -> B4
                "b5" -> B5
                "b6" -> B6
                "b7" -> B7
                "b8" -> B8
                "b9" -> B9
                "b10" -> B10
                "c0" -> C0
                "c1" -> C1
                "c2" -> C2
                "c3" -> C3
                "c4" -> C4
                "c5" -> C5
                "c6" -> C6
                "c7" -> C7
                "c8" -> C8
                "c9" -> C9
                "c10" -> C10
                "d1" -> D1
                "d2" -> D2
                "d3" -> D3
                "d4" -> D4
                "d5" -> D5
                "d6" -> D6
                "d7" -> D7
                "e3" -> E3
                "e4" -> E4
                "e5" -> E5
                "e6" -> E6
                "letter" -> LETTER
                "legal" -> LEGAL
                "longbond" -> LONGBOND
                "tabloid" -> TABLOID
                else -> null
            }
        }
    }
}

/**
 * The DocSize class handles all the nastiness of dealing with typographic units, default document
 * sizes, and so on. Because of the large number of differing Imperial-based standards, this class
 * uses millimeters as its standard and tracks values internally as micrometers to avoid rounding
 * errors.
 */
class DocSize(var width: DocUnit, var height: DocUnit) {

    fun isPortrait(): Boolean { return width.value < height.value }

    fun rotate(): DocSize {
        width = height.also { height = width }
        return this
    }

    companion object {

        /**
         * Returns a DocSize instance based on the specified document size. Note that all
         * document sizes are in portrait orientation and a call to `rotate()` will be needed
         * to change orientation to any objects instantiated from this method.
         */
        fun fromPreset(size: DocSizePreset): DocSize {
            return when (size) {
                DocSizePreset.A0 -> DocSize(DocUnit(841000), DocUnit(1189000))
                DocSizePreset.A1 -> DocSize(DocUnit(594000), DocUnit(841000))
                DocSizePreset.A2 -> DocSize(DocUnit(420000), DocUnit(594000))
                DocSizePreset.A3 -> DocSize(DocUnit(297000), DocUnit(420000))
                DocSizePreset.A4 -> DocSize(DocUnit(210000), DocUnit(297000))
                DocSizePreset.A5 -> DocSize(DocUnit(148000), DocUnit(210000))
                DocSizePreset.A6 -> DocSize(DocUnit(105000), DocUnit(148000))
                DocSizePreset.A7 -> DocSize(DocUnit(74000), DocUnit(105000))
                DocSizePreset.A8 -> DocSize(DocUnit(52000), DocUnit(74000))
                DocSizePreset.A9 -> DocSize(DocUnit(37000), DocUnit(52000))
                DocSizePreset.A10 -> DocSize(DocUnit(26000), DocUnit(37000))
                DocSizePreset.B0 -> DocSize(DocUnit(1000000), DocUnit(1414000))
                DocSizePreset.B1 -> DocSize(DocUnit(707000), DocUnit(1000000))
                DocSizePreset.B2 -> DocSize(DocUnit(500000), DocUnit(707000))
                DocSizePreset.B3 -> DocSize(DocUnit(353000), DocUnit(500000))
                DocSizePreset.B4 -> DocSize(DocUnit(250000), DocUnit(353000))
                DocSizePreset.B5 -> DocSize(DocUnit(176000), DocUnit(250000))
                DocSizePreset.B6 -> DocSize(DocUnit(125000), DocUnit(176000))
                DocSizePreset.B7 -> DocSize(DocUnit(88000), DocUnit(125000))
                DocSizePreset.B8 -> DocSize(DocUnit(62000), DocUnit(88000))
                DocSizePreset.B9 -> DocSize(DocUnit(44000), DocUnit(62000))
                DocSizePreset.B10 -> DocSize(DocUnit(31000), DocUnit(44000))
                DocSizePreset.C0 -> DocSize(DocUnit(917000), DocUnit(1297000))
                DocSizePreset.C1 -> DocSize(DocUnit(648000), DocUnit(917000))
                DocSizePreset.C2 -> DocSize(DocUnit(458000), DocUnit(648000))
                DocSizePreset.C3 -> DocSize(DocUnit(324000), DocUnit(458000))
                DocSizePreset.C4 -> DocSize(DocUnit(229000), DocUnit(324000))
                DocSizePreset.C5 -> DocSize(DocUnit(162000), DocUnit(229000))
                DocSizePreset.C6 -> DocSize(DocUnit(114000), DocUnit(162000))
                DocSizePreset.C7 -> DocSize(DocUnit(81000), DocUnit(114000))
                DocSizePreset.C8 -> DocSize(DocUnit(57000), DocUnit(81000))
                DocSizePreset.C9 -> DocSize(DocUnit(40000), DocUnit(57000))
                DocSizePreset.C10 -> DocSize(DocUnit(28000), DocUnit(40000))
                DocSizePreset.D1 -> DocSize(DocUnit(545000), DocUnit(771000))
                DocSizePreset.D2 -> DocSize(DocUnit(385000), DocUnit(545000))
                DocSizePreset.D3 -> DocSize(DocUnit(272000), DocUnit(385000))
                DocSizePreset.D4 -> DocSize(DocUnit(192000), DocUnit(272000))
                DocSizePreset.D5 -> DocSize(DocUnit(136000), DocUnit(192000))
                DocSizePreset.D6 -> DocSize(DocUnit(96000), DocUnit(136000))
                DocSizePreset.D7 -> DocSize(DocUnit(68000), DocUnit(96000))
                DocSizePreset.E3 -> DocSize(DocUnit(400000), DocUnit(560000))
                DocSizePreset.E4 -> DocSize(DocUnit(280000), DocUnit(400000))
                DocSizePreset.E5 -> DocSize(DocUnit(200000), DocUnit(280000))
                DocSizePreset.E6 -> DocSize(DocUnit(140000), DocUnit(200000))
                DocSizePreset.LETTER -> DocSize(DocUnit(215900), DocUnit(279400))
                DocSizePreset.LEGAL -> DocSize(DocUnit(215900), DocUnit(355600))
                DocSizePreset.LONGBOND -> DocSize(DocUnit(215900), DocUnit(330200))
                DocSizePreset.TABLOID -> DocSize(DocUnit(279400), DocUnit(431800))
            }
        }

        /**
         * Returns a DocSize based on a string passed to it. In addition to the string versions
         * of the existing size presets ("a4", "letter", etc.) the suffix "-landscape" can be
         * appended to a preset string to obtain the same document size in landscape orientation,
         * e.g. "a4" vs "a4-landscape" or "letter" vs "letter-landscape".
         */
        fun fromString(string: String): DocSize? {
            var landscape = false
            var sizeString = string
            if (string.endsWith("-landscape")) {
                landscape = true
                sizeString = string.split("-")[0]
            }

            val out = fromPreset(DocSizePreset.fromString(sizeString) ?: return null)
            if (landscape) out.rotate()
            return out
        }
    }
}

/**
 * The DocUnit class provides an abstract interface to typographic measures of length.
 * Internally it uses micrometers as a standard to avoid rounding errors where possible.
 */
class DocUnit(var value: Long = 0) {
    fun setInches(size: Double): DocUnit { this.value = (size * 25_400.0).toLong(); return this }
    fun setMM(size: Double): DocUnit { this.value = (size * 1000.0).toLong(); return this }
    fun setPoints(size: Double): DocUnit { this.value = (size * 353.0).toLong(); return this }
    fun setPicas(size: Double): DocUnit { this.value = (size * 4233.0).toLong(); return this }

    fun toInches(): Double { return value / 25_400.0 }
    fun toMM(): Double { return value / 1000.0 }
    fun toPoints(): Double { return value / 353.0 }
    fun toPicas(): Double { return value / 4233.0 }

    companion object {
        fun fromString(string: String): DocUnit? {
            val temp = string.trim().lowercase()
            if (!reDocUnitFormat.matches(temp)) return null

            val units = temp.drop(temp.length - 2)
            val value = temp.dropLast(2).trimEnd().toDoubleOrNull() ?: return null

            return when (units) {
                "in" -> DocUnit().setInches(value)
                "mm" -> DocUnit().setMM(value)
                "pc" -> DocUnit().setPicas(value)
                "pt" -> DocUnit().setPoints(value)
                else -> null
            }
        }
    }
}
