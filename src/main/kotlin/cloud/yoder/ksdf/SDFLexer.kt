package cloud.yoder.ksdf

class SDFLexer {
    var tokens = ArrayList<Token>()
        private set
    private var insideTag = false
    private var stream = PeekCharStream("")
    private var lineNumber = 1

    fun process(input: String): SDFLexer {
        stream.set(input)
        tokens.clear()

        while (true) {
            if (stream.getCurrent() == null) {
                tokens.add(Token(TokenType.EOF, "", lineNumber))
                break
            }

            val c = stream.getCurrent()!!

            // This switch handles the easy stuff: delimiter characters
            if (c == '[') {
                tokens.add(Token(TokenType.LBRACKET, "[", lineNumber))
                insideTag = true
                stream.moveNext()
                continue
            }

            // We made it this far in the loop, so it means that it wasn't a delimiting character.
            // Now we get into the more extended processing.
            if (!insideTag) {
                // readContent() consumes all non-markup text. The parser doesn't have to be
                // concerned with user content.
                readContent()
                continue
            }

            // Dump binary data. The parser doesn't even need to know about it.
            if (isIllegal(c)) {
                stream.moveNext()
                continue
            }

            // From here on out to the end of the loop is just processing stuff inside tags
            when (c) {
                '/' -> {
                    tokens.add(Token(TokenType.SLASH, "/", lineNumber))
                    stream.moveNext()
                    continue
                }
                '"' -> {
                    readString()
                    continue
                }
                '=' -> {
                    tokens.add(Token(TokenType.EQ, "=", lineNumber))
                    stream.moveNext()
                    continue
                }
                ']' -> {
                    tokens.add(Token(TokenType.RBRACKET, "]", lineNumber))
                    insideTag = false
                    stream.moveNext()
                    continue
                }
            }

            if (Character.valueOf(c).isWhitespace()) {
                dumpWhitespace()
                continue
            }

            if (isLeadIDChar(c)) {
                readID()
                continue
            }

            // Uh-oh. We got this far, which means there's a problem. The best we can do here is
            // treat everything as regular content until we find another bracket.
            readContent()
        }

        return this
    }

    override fun toString(): String {
        val sb = StringBuilder("SDFLexer{")
        tokens.forEach { sb.append("\n    $it") }
        sb.append("\n}")

        return sb.toString()
    }

    // reads in an identifier token from the current position. This is either (a) a tag name or (b)
    // an attribute name.
    private fun readID() {
        val startPos = stream.getPosition()
        var endPos =  stream.getPosition()

        while (stream.hasNext()) {
            endPos = stream.getPosition()
            if (!isIDChar(stream.peekNext()!!)) {
                stream.moveNext()
                break
            }
            stream.moveNext()
        }

        tokens.add(Token(TokenType.IDENT, stream.data.substring(startPos, endPos+1), lineNumber))
    }

    // reads in a string.
    private fun readString() {

        if (stream.getCurrent() != '"') {
            // We should *never* be here
            throw IllegalStateException()
        }

        val strList = StringBuilder()

        val tokenLine = lineNumber
        while (stream.moveNext() != null) {

            if (isIllegal(stream.getCurrent()!!)) {
                stream.moveNext()
                continue
            }

            when (stream.getCurrent()) {
                '\n' -> {
                    lineNumber++
                    strList.append(stream.getCurrent())
                }
                '\\' -> {
                    if (stream.hasNext())
                        strList.append(stream.moveNext()!!)
                    else
                        strList.append(stream.getCurrent())
                }
                '"' -> {
                    stream.moveNext()
                    break
                }
                else -> {
                    // character isn't escaped or the end of the string, so add it to the list
                    strList.append(stream.getCurrent())
                }
            }
        }

        tokens.add(Token(TokenType.STRING, strList.toString(), tokenLine))
    }

    private fun readContent() {

        val strList = StringBuilder()

        var tokenLine = lineNumber
        var leadingWhitespace = true
        while (stream.getCurrent() != null) {

            if (isIllegal(stream.getCurrent()!!)) {
                stream.moveNext()
                continue
            }

            when (stream.getCurrent()) {
                '\n' -> {
                    lineNumber++
                    if (leadingWhitespace) tokenLine++
                    strList.append(stream.getCurrent())
                }
                '\\' -> {
                    leadingWhitespace = false
                        if (stream.hasNext())
                        strList.append(stream.moveNext()!!)
                    else
                        strList.append(stream.getCurrent())
                }
                '[' -> {
                    break
                }
                else -> {
                    // character isn't escaped or the end of the string, so add it to the list
                    leadingWhitespace = false
                    strList.append(stream.getCurrent())
                }
            }
            stream.moveNext()
        }

        tokens.add(Token(TokenType.CONTENT, strList.toString(), tokenLine))
    }

    private fun dumpWhitespace() {
        while (stream.getCurrent() != null) {
            if (stream.getCurrent() == '\n') {
                lineNumber++
            } else if (!Character.valueOf(stream.getCurrent()!!).isWhitespace()) {
                break
            }
            stream.moveNext()
        }
    }
}

private fun isLeadIDChar(c: Char): Boolean {
    return c in 'a'..'z'
}

private fun isIDChar(c: Char): Boolean {
    return c in 'a'..'z' || c in '0'..'9' || c == '_'
}

private fun isIllegal(c: Char): Boolean {
    return  c in '\u0000'..'\u0008' ||
            c in '\u000b'..'\u000c' ||
            c in '\u000e'..'\u001f' ||
            c == '\u007f'
}