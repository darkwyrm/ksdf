package cloud.yoder.ksdf

import org.junit.Test

class ParserLintTest {

    @Test
    fun badNestingTest() {
        val document = """[document][body][b][i][/b][/i][/body][/document]"""
        val parser = SDFParser().process(SDFLexer().process(document).tokens)
        assert(parser.messages.size == 1)
        assert(parser.messages[0].code == ParserCode.BAD_NESTING)
    }

    @Test
    fun badNestingTest2() {
        val document = "[document][body][h1]Heading 1[/h1]\n" +
                "[b]bold[i]bolditalic[/b]italic[/i][/body][/document]"
        val parser = SDFParser().process(SDFLexer().process(document).tokens)
        assert(parser.messages.size == 1)
        assert(parser.messages[0].code == ParserCode.BAD_NESTING)
    }

    @Test
    fun badParamTest1() {
        val document = """[document param=][/document]"""
        val parser = SDFParser().process(SDFLexer().process(document).tokens)
        assert(parser.messages.size == 1)
        assert(parser.messages[0].code == ParserCode.IGNORED_CONTENT)
    }

    @Test
    fun badParamTest2() {
        val document = """[document param=bad][/document]"""
        val parser = SDFParser().process(SDFLexer().process(document).tokens)
        assert(parser.messages.size == 1)
        assert(parser.messages[0].code == ParserCode.BAD_PARAMETER)
    }

    @Test
    fun unclosedTagTest() {
        val document = """[document][body][/body[/document]"""
        val parser = SDFParser().process(SDFLexer().process(document).tokens)
        assert(parser.messages.size == 1)
        assert(parser.messages[0].code == ParserCode.IGNORED_CONTENT)
    }

}