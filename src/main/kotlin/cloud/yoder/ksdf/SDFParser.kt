package cloud.yoder.ksdf

class SDFParser {
    var root = rootNode()
        private set
    private var tokens = ArrayList<Token>()
    var messages = ArrayList<ParserMsg>()
        private set

    // Easy access for common structural tags
    var docTag: Node? = null
    var sigTag: Node? = null
    var encTag: Node? = null

    var headTag: Node? = null
    var bodyTag: Node? = null
    var attTag: Node? = null

    /**
     * Takes the tokens passed to the instance and creates a node tree from them. This call
     * clears any existing state for the instance. When this call returns, any problems
     * processing the document will be found in the `messages` property.
     */
    fun process(tokenList: ArrayList<Token>): SDFParser {
        root = rootNode()
        tokens = tokenList
        messages.clear()

        var i = 0
        var currentTag = root
        while (i < tokens.size) {
            val token = tokens[i]
            when (token.type) {
                TokenType.LBRACKET -> {
                    val endIndex = getTagEnd(i)
                    if (endIndex < 0) {
                        // i will be negative if recoverUnclosedTag() handled the problem for us
                        i = recoverUnclosedTag(i)
                        if (i < 0) continue
                    }

                    currentTag = processTag(currentTag, i, endIndex)

                    if (currentTag.type == NodeType.TAG) {
                        when (currentTag.token.value) {
                            "document" -> docTag = currentTag
                            "head" -> headTag = currentTag
                            "body" -> bodyTag = currentTag
                            "attachments" -> attTag = currentTag
                            "signature" -> sigTag = currentTag
                            "encryption" -> encTag = currentTag
                        }
                    }

                    i = endIndex
                }
                TokenType.CONTENT -> currentTag.addChild(textNode(token))
                TokenType.ILLEGAL -> { /* skip over illegal tokens */ }
                TokenType.EOF -> break
                else -> messages.add(
                    pError("Unimplemented token ${token.value}",
                    ParserCode.UNRECOGNIZED_TOKEN, token.line)
                )
            }

            i++
        }

        return this
    }

    /**
     * Once a node tree has been created, this call checks to make sure the document it
     * represents is valid, verifying tag names and parameters and so on.
     */
    fun validate(): SDFParser {

        // We'll start validating the document by ensuring that it has, at minimum, a document
        // tag and a body tag. If it has some of the other structural tags (head, attachments,
        // etc.) we'll also set up easy access to them, as well.

        if (root.children.isEmpty()) {
            error("Empty document", ParserCode.EMPTY_DOCUMENT, -1, null)
            return this
        }

        for (child in root.children) {
            if (child.type == NodeType.TEXT) {
                if (reWhitespace.matches(child.token.value)) {
                    root.children.remove(child)
                } else {
                    warn("Ignoring extra content inside root node",
                        ParserCode.IGNORED_CONTENT, child.token.line, child)
                }
                continue
            }

            when (child.token.value) {
                "document" -> docTag = child
                "signature" -> sigTag = child
                "encryption" -> encTag = child
                else -> {
                    error("Bad location for '${child.token.value}'. Only 'document', " +
                            "'signature', and 'encryption' may be found in the root node",
                        ParserCode.BAD_TAG_LOCATION, child.token.line, child)
                }
            }
        }

        // SDF/SFTM documents must have at minimum a document tag and a body tag. Not having them
        // is a fatal error.

        when (countChildTag(root, "document", true)) {
            1 -> { /* do nothing */ }
            0 -> {
                fatal("No document tag found", ParserCode.BAD_DOCUMENT_TAG_COUNT, -1, null)
                return this
            }
            else -> {
                fatal("Multiple document tags found", ParserCode.BAD_DOCUMENT_TAG_COUNT,
                    docTag!!.token.line, null)
                return this
            }
        }

        when (countChildTag(docTag!!, "body", true)) {
            1 -> { /* do nothing */ }
            0 -> {
                fatal("No body tag found", ParserCode.BAD_BODY_TAG_COUNT, -1, null)
                return this
            }
            else -> {
                fatal("Multiple body tags found", ParserCode.BAD_BODY_TAG_COUNT,
                    bodyTag!!.token.line, null)
                return this
            }
        }

        // Main structural nodes have been identified. Now let's validate them, too

        validateSubtags(docTag, listOf("head", "body", "attachments"))
        validateSubtags(headTag, listOf("meta", "title", "style"))
        validateSubtags(attTag, listOf("attachment"))

        if (headTag != null && countChildTag(headTag!!, "title", true) > 1) {
            error("Multiple title tags found", ParserCode.MULTIPLE_TITLE_TAGS, -1, headTag)
        }

        return this
    }

    /**
     * This optional call is made after calling validate() and automatically handles many
     * warnings and errors. Messages which are corrected are removed from the list. Messages
     * about ignored content are not removed so that it is known what content has been ignored.
     */
    fun makeCompliant(): SDFParser {
        for (msg in messages) {

            when (msg.code) {
                // Codes which are resolved just by removing the associated node
                ParserCode.ILLEGAL_TAG_NAME, ParserCode.BAD_TAG_LOCATION -> {
                    if (msg.node != null) {
                        if (msg.node!!.parent != null) {
                            msg.node!!.parent!!.children.remove(msg.node)
                            messages.remove(msg)
                        }
                    }
                }
                else -> { /* Do nothing */ }
            }
        }
        return this
    }

    private fun getTagEnd(start: Int): Int {
        // This method just figures out which tokens are part of the tag and returns the index of
        // the right bracket tag that corresponds to the left bracket tag given to it. If there
        // is no corresponding closing bracket, it returns -1.
        for (i in start..tokens.size) {
            if (tokens[i].type == TokenType.RBRACKET)
                return i
        }
        return -1
    }

    private fun recoverUnclosedTag(start: Int): Int {
        // It should push an UNCLOSED_TAG error and do more to try to recover from the problem.

        /*
        This is the kinda-hard bit. If we're here, it means that the tag wasn't closed. We
        can't make too many assumptions, but if the lexer encounters anything which doesn't
        belong inside a tag, it will start turning what it's encountered into a Content tag until
        the next bracket is encountered. This will make unclosed tags painfully obvious, which
        is a Good Thing™. The solution, then, is to search for the next opening bracket, work
        backwards until we find a Content token and stick a closing bracket right before it. If
        we don't find one and end up back at the original opening bracket, then we'll just turn
        the contents leading up to the next opening bracket into a Content tag, which will also
        dramatically display the error. Big, obvious mistakes are a lot easier to find and
        correct than little, subtle ones. 😏
         */
        var nextLB = -1
        for (i in start..tokens.size) {
            if (tokens[i].type == TokenType.LBRACKET) {
                nextLB = i
                break
            }
        }

        val lineNumber = tokens[start].line
        if (nextLB != -1) {
            var index = nextLB
            while (index > start) {
                if (tokens[index].type == TokenType.CONTENT) {
                    tokens.add(index, Token(TokenType.RBRACKET, "]", lineNumber))
                    return index
                }
                index--
            }
        }

        // We got this far, so it means that we need to turn the current opening bracket and all
        // tokens up to the next one into a content tag, add it to the document tree, and return
        // -1 to indicate that the parser needs to skip to the next token.
        val sb = StringBuilder()
        for (i in start until nextLB) {
            sb.append(tokens[i].value)
        }

        while (tokens[start].type != TokenType.LBRACKET)
            tokens.removeAt(start)
        tokens.add(start, Token(TokenType.CONTENT, sb.toString(), lineNumber))

        return -1
    }

    private fun processTag(parent: Node, start: Int, end: Int): Node {
        var index = start + 1
        if (index == end) {
            // Empty tag. Don't do anything. Technically it's an error, but it's not a big deal.
            warn("Empty tag", ParserCode.EMPTY_TAG, tokens[index].line, null)
            return parent
        }

        var currentToken = tokens[index]
        var isClosing = false
        if (currentToken.type == TokenType.SLASH) {
            isClosing = true
            index++
            currentToken = tokens[index]
        }

        if (currentToken.type != TokenType.IDENT) {
            messages.add(
                pError("Bad tag name '${currentToken.value}'",
                ParserCode.UNRECOGNIZED_TOKEN, currentToken.line)
            )
            return parent
        }

        if (isClosing) {
            if (tokens[index + 1].type != TokenType.RBRACKET) {
                warn("Ignoring extra content in closing tag '${tokens[start+1].value}'",
                    ParserCode.IGNORED_CONTENT, tokens[index+1].line, null)
            }

            val lastOpen = parent.findLastOpenTag(currentToken.value)
            if (lastOpen == null) {
                warn("Unopened closing tag '${tokens[start].value}'",
                    ParserCode.UNOPENED_TAG_PAIR, tokens[index+1].line, null)
                return parent
            }

            lastOpen.isClosed = true

            return if (lastOpen === parent) {
                parent.findOpenParent() ?: parent.parent!!
            } else {
                warn("Closing tag '/${tokens[start+2].value}' should be inside the " +
                        "same tag pair as its partner", ParserCode.BAD_NESTING,
                    tokens[index+1].line, null)
                parent
            }
        }

        // We got this far, so we have the information needed to create a tag node
        val tag = tagNode(tokens[index])
        index++
        currentToken = tokens[index]

        // Process tag parameters
        while (index < end && currentToken.type == TokenType.IDENT) {
            when (end) {
                index + 1 -> {
                    warn("Ignoring invalid content '${currentToken.value}'",
                        ParserCode.IGNORED_CONTENT, currentToken.line, null)
                    break
                }
                index + 2 -> {
                    val badContent = currentToken.value + tokens[index+1].value
                    warn("Ignoring invalid content '$badContent'",
                        ParserCode.IGNORED_CONTENT, currentToken.line, null)
                    break
                }
            }

            // If we got this far, it means we have a triplet of content with an identifier at
            // index. Make sure we have the right token types and, if so, process the triplet as
            // a tag parameter
            if (tokens[index+1].type != TokenType.EQ || tokens[index+2].type != TokenType.STRING) {
                val badContent = currentToken.value + tokens[index + 1].value +
                        tokens[index + 2].value
                warn("Ignoring invalid tag parameter '$badContent'",
                    ParserCode.BAD_PARAMETER,  currentToken.line, null)
                index += 3
                currentToken = tokens[index]
                continue
            }
            tag.params[currentToken.value] = tokens[index+2].value

            index += 3
            currentToken = tokens[index]
        }

        parent.addChild(tag)
        return tag
    }

    private fun validateSubtags(parent: Node?, tags: List<String>) {
        if (parent == null) return

        val removeList = mutableListOf<Pair<Node, Node>>()
        for (child in parent.children) {

            if (child.type == NodeType.TEXT) {
                if (reWhitespace.matches(child.token.value)) {
                    removeList.add(Pair(parent, child))
                } else {
                    warn("Ignoring extra content inside ${parent.token.value} tag",
                        ParserCode.IGNORED_CONTENT, child.token.line, null)
                }
                continue
            }

            if (child.token.value !in tags) {
                error("Tag '${child.token.value}' not permitted inside a " +
                    "${parent.token.value} tag and will be ignored/removed.",
                    ParserCode.BAD_TAG_LOCATION, child.token.line, child)
            }
        }

        for (item in removeList)
            item.first.children.remove(item.second)
    }

    private fun warn(msg: String, code: ParserCode, line: Int, node: Node?) {
        val msgItem = ParserMsg(MessageType.WARNING, code, "$msg in line $line", line)
        msgItem.node = node
        messages.add(msgItem)
    }

    private fun error(msg: String, code: ParserCode, line: Int, node: Node?) {
        val msgItem = ParserMsg(MessageType.ERROR, code, "$msg in line $line", line)
        msgItem.node = node
        messages.add(msgItem)
    }

    private fun fatal(msg: String, code: ParserCode, line: Int, node: Node?) {
        val msgItem = ParserMsg(MessageType.FATAL, code, "$msg in line $line", line)
        msgItem.node = node
        messages.add(msgItem)
    }

    companion object {
        /**
         * A convenience function which takes a string of file data and returns an SDFParser which has
         * processed the document into a Node tree.
         */
        fun parseFromString(document: String): SDFParser {
            return SDFParser().process(SDFLexer().process(document).tokens).validate().makeCompliant()
        }
    }
}
