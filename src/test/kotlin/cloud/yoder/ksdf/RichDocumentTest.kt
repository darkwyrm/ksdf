package cloud.yoder.ksdf

import cloud.yoder.ksdf.style.Style
import org.junit.Test
import javafx.scene.paint.Color

class RichDocumentTest {

    @Test
    fun styleAppendTest() {
        val style = Style()
        style.setItalic(true)
        style.setTextColor(Color.rgb(75,75,100))

        val from = Style()
        from.setBold(true)
        from.setBGColor(Color.rgb(200, 200, 200))

        style.append(from)
        assert(style.properties.containsKey("font-weight"))
        assert(style.properties.containsKey("background-color") && style.properties["background-color"] == "rgb(200, 200, 200)")
    }

}