package cloud.yoder.ksdf

enum class TokenType {
    // Tag components
    LBRACKET,
    RBRACKET,
    IDENT,
    SLASH,
    EQ,

    // whitespace and newlines. They don't matter outside the message body, but they are significant inside, so we
    // need to be able to track these things
    WS,
    LF,

    // Tag attributes use a very basic string format
    STRING,

    // All text that is not explicitly handled as SDF structure is document content
    CONTENT,

    // Escape codes for both brackets and the double quote
    ESC,

    // Illegal content:
    // - Characters with an ASCII value of < 32, excepting tab, cr, and lf
    // - Non-UTF8 data, such as raw binary data
    ILLEGAL,

    // Used for the root node in a document
    EMPTY,

    EOF;
}

data class Token(val type: TokenType, val value: String, val line: Int)