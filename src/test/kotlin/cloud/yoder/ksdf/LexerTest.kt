package cloud.yoder.ksdf

import org.junit.Test

class LexerTest {

    // This just tests lexing of a single, simple opening tag with whitespace handling
    @Test
    fun openTagTest1() {
        val lexer = SDFLexer().process("[ document\t]")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests handling of attributes
    @Test
    fun openTagTest2() {
        val lexer = SDFLexer().process("[document type=\"sdf\"]")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.IDENT, "type", 1),
            Token(TokenType.EQ, "=", 1),
            Token(TokenType.STRING, "sdf", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests handling of escape codes inside tag parameters
    @Test
    fun escapeTest1() {
        val lexer = SDFLexer().process("""[document param="a \" b"]""")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.IDENT, "param", 1),
            Token(TokenType.EQ, "=", 1),
            Token(TokenType.STRING, """a " b""", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))

        // If someone accidentally escapes the ending quote in a tag parameter value, the value
        // should literally run to the next quote or the end of the document, whichever comes first.
        lexer.process("""[document param="a \"]a""")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.IDENT, "param", 1),
            Token(TokenType.EQ, "=", 1),
            Token(TokenType.STRING, """a "]a""", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This just tests lexing of a pair of tags
    @Test
    fun tagPairTest1() {
        val lexer = SDFLexer().process("[document][ /document]")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.SLASH, "/", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests handling of tag pair content
    @Test
    fun contentTest1() {
        val lexer = SDFLexer().process("[body]test[/body]")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.CONTENT, "test", 1),
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.SLASH, "/", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))

        lexer.process("""[body]foo\[bar[/body]""")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.CONTENT, "foo[bar", 1),
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.SLASH, "/", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests illegal content inside tags
    @Test
    fun illegalTest1() {
        val lexer = SDFLexer().process("test\u0000\u0001foo")
        assert(lexer.tokens == listOf(
            Token(TokenType.CONTENT, "testfoo", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests illegal content inside tags
    @Test
    fun unclosedTagTest() {
        val lexer = SDFLexer().process("[body 123[/body]")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.CONTENT, "123", 1),
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.SLASH, "/", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests content that actually resembles a real document
    @Test
    fun fullTest1() {
        val lexer = SDFLexer().process(
        """[document type="sftm" version="1.0"][body]This is some content[/body][/document]""")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "document", 1),

            Token(TokenType.IDENT, "type", 1),
            Token(TokenType.EQ, "=", 1),
            Token(TokenType.STRING,"sftm", 1),

            Token(TokenType.IDENT, "version", 1),
            Token(TokenType.EQ, "=", 1),
            Token(TokenType.STRING,"1.0", 1),

            Token(TokenType.RBRACKET, "]", 1),

            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),

            Token(TokenType.CONTENT, "This is some content", 1),

            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.SLASH, "/", 1),
            Token(TokenType.IDENT, "body", 1),
            Token(TokenType.RBRACKET, "]", 1),

            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.SLASH, "/", 1),
            Token(TokenType.IDENT, "document", 1),
            Token(TokenType.RBRACKET, "]", 1),
            Token(TokenType.EOF, "", 1),
        ))
    }

    // This tests line number handling
    @Test
    fun lineNumTest() {
        val lexer = SDFLexer().process("[\nbody value=\"foo\nbar\"]\ntext\n[/body]")
        assert(lexer.tokens == listOf(
            Token(TokenType.LBRACKET, "[", 1),
            Token(TokenType.IDENT, "body", 2),
            Token(TokenType.IDENT, "value", 2),
            Token(TokenType.EQ, "=", 2),
            Token(TokenType.STRING, "foo\nbar", 2),
            Token(TokenType.RBRACKET, "]", 3),
            Token(TokenType.CONTENT, "\ntext\n", 4),
            Token(TokenType.LBRACKET, "[", 5),
            Token(TokenType.SLASH, "/", 5),
            Token(TokenType.IDENT, "body", 5),
            Token(TokenType.RBRACKET, "]", 5),
            Token(TokenType.EOF, "", 5),
        ))
    }
}

class CharStreamTest {

    @Test
    fun testCharStream() {
        val s = PeekCharStream("abc")

        assert(s.getCurrent() == 'a')
        assert(s.hasNext())
        assert(s.peekNext() == 'b')

        assert(s.moveNext() == 'b')
        assert(s.hasNext())
        assert(s.peekNext() == 'c')

        assert(s.moveNext() == 'c')
        assert(!s.hasNext())
        assert(s.peekNext() == null)
    }
}
